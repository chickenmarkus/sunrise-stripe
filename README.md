# Sunrise simulation with a LED stripe #

Sunrise simulation with a LED stripe controlled by a Arduino-compatible micro-controller when powered on.

The LED stripe is controlled by the `Adafruit_NeoPixel`-library. Thus, it can be a SK6812 based RGBW chipset, for instance.



## IMPORTANT ##

To reduce NeoPixel burnout risk, add 1000 uF capacitor across pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input and minimize distance between Arduino and first pixel. Avoid connecting on a live circuit...if you must, connect GND first.


### Note ###

RGBW LEDs draw up to 80mA with all colors + white at full brightness!
That means that a 60-pixel strip can draw up to 60x 80 = 4800mA, so you should use a 5A power supply; for a 144-pixel strip its max 11520mA = 12A!



## Example ##

* DigiSpark with microUSB (less than $2)
* 1 meter SK6812 stripe with 60 LEDs (approx. $10)
* 5V/3A power supply with microUSB (less than $3)
  *ATTENTION: This is not sufficient for powering the four colors of all LEDs with maximum brightness! Indeed, this is not the case.*

Finally, the power supply goes into the DigiSpark, the LED stripe is soldered to 5V & GND (buffered by a capacitor) and a data pin (with resistor in line) of the DigiSpark.



## Implementation ##

The sun is a circle with the diameter of the LED stripe length `NUMPIXELS` (default 60).
This is the core of the sun which shines white (color temperature `TEMP_CORE`, default 5900 K).
Additionally, there es a surrounding of width `NUMTRANSITION` (default 30) added where the color temperature fades from `TEMP_CORE` to `TEMP_EDGE` (default 500 K).

The circle rises from below the horizon which is represented by the LED stripe.
The LEDs reflect the colors of the intersection of the horizon and the circle.
If the shape has risen to its full height, the intersection is exactly the diameter of the core and, thus, all LEDs are fully white.
At this position, the circle stops rising so that all LEDs keep white.

Because of the circle as shape, the colors are primarily separated in time while the colors of the LED pixels are always similar to eachother.
Alternatively, the circle can be replaced by a triangle via defining the macro `TRIANGLE`.
In this case, the colors are primarily separated in space while many colors are present at the same time.
However, this exludes `TEMP_EDGE` significantly when using indirect lumination because the separate LED pixels are mixed to one average color.


### Additional features ###

* `NUMFADE` (default 3): fade in newly powered on LEDs (outer ring within `NUMTRANSITION`)
* `BOOST`: boost the white LED by additional white via RGB


### Potential useful changes ###

* HSI to RGBW using [SaikoLED](https://blog.saikoled.com/post/44677718712/how-to-convert-from-hsi-to-rgb-white): How to convert the color temperature (CCT) to HSI?



## References ##

The code is significantly based on [RGBW LED Strip with generic controller (f.e. SK6812RGBW)](https://forum.arduino.cc/index.php?topic=364208.0).

The conversion from the color temperature (CCT) to the RGBW values is taken from [Susensio's RGBWLed](https://github.com/Susensio/RGBWLed/).

