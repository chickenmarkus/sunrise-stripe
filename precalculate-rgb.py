#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  
#  Copyright 2020  <Chickenmarkus>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


"""
Pre-calculate the CCT-to-RGB conversion from https://github.com/Susensio/RGBWLed/
"""

import numpy as np
import matplotlib.pyplot as plt

# config
temperature = np.linspace(500, 6000, 256)  # temperature range [Kelvin]
numbers_per_line = 32
colors = ["red", "green", "blue"]



def calculate(temp=None):
    temp = (temp or temperature) / 100
    rgb = np.empty((len(temp), 3))  # [temp, R/G/B]

    # calculate red
    rgb[:, 0] = 329.698727446 * (temp-60)**-0.1332047592
    rgb[temp<=66, 0] = 255

    # calculate green
    args = temp < 66
    rgb[args, 1] = 99.4708025861 * np.log(temp[args]) - 161.1195681661
    args = ~args
    rgb[args, 1] = 288.1221695283 * (temp[args]-60)**-0.0755148492

    # calcluate blue
    rgb[:, 2] = 138.5177312231 * np.log(temp-10) - 305.0447927307
    rgb[temp<=19, 2] = 0
    rgb[66<temp, 2] = 255

    # constrain to 0..255
    rgb[rgb<0] = 0
    rgb[rgb>255] = 255
    rgb = rgb.astype(np.uint8)

    return rgb



def plot(rgb):
    fig, ax = plt.subplots()
    ax.set_xlabel("temperature [Kelvin]")
    ax.set_ylabel("value")
    for (c, color) in enumerate(colors):
        ax.plot(temperature, rgb[:, c], label=color, color=color)
    ax.legend()
    plt.show()



def output(rgb):
    print("  static const uint8_t rgb[3][{}] PROGMEM = {{ // from {} to {} Kelvin".format(
            len(rgb), int(temperature[0]), int(temperature[-1])))
    for (c, color) in enumerate(colors):
        print("    {{ // {}".format(color))
        for l in range(0, len(rgb), numbers_per_line):
            print("      "
                    + ", ".join(rgb[l:l+numbers_per_line, c].astype(np.str))
                    + "," )
        print("    },")
    print("  };")



def main():
    rgb = calculate()
    output(rgb)
    plot(rgb)
    return 0



if __name__ == '__main__':
    main()

