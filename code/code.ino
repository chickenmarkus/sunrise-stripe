// sunrise simulation with a LED stripe
// original code from:
// https://forum.arduino.cc/index.php?topic=364208.0


#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif
#include <math.h>
#include <avr/pgmspace.h>


// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define NEO_PIN     0  // NeoPixel DATA
#ifdef __AVR_ATtiny85__
#define LED_BUILTIN 0 // LED on Model B
//#define LED_BUILTIN   1 // LED on Model A
#endif

// What type of NeoPixel strip is attached to the Arduino?
#define NEO_PTYPE   NEO_GRBW  // f.e. SK6812
//#define NEO_PTYPE NEO_GRB   // most NeoPixel products

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS       60
#define NUMTRANSITION   (NUMPIXELS/2) // additional surrounding for the color transition
#define NUMFADE         3             // width of fading in NUMTRANSITION

// How long shall the sunrise take?
#define DURATION    1200000   // milli-seconds[ms]

// color
#define BRIGHTNESS  255       // set max brightness 0..255
#define TEMP_EDGE   500       // start color temperature [Kelvin]
#define TEMP_CORE  5000       // target color temperature [Kelvin]

// Shall the sun have the shape of a triangle instead of a circle?
//#define TRIANGLE              // rising triangle instead of the circle

// miscellaneous
#define TIMESCALE   32        // maximum update frequency [Hz] = 1000/TIMESCALE
#define FLOAT       128       // scaling factor to avoid floating point operations
#define UNITS_PER_PIXEL 512   // even number, total steps of sunrise = UNITS_PER_PIXEL * ((NUMPIXELS-1)/2 + NUMTRANSITION) = R
#define R   ((NUMPIXELS-1)*UNITS_PER_PIXEL/2 + NUMTRANSITION*UNITS_PER_PIXEL) // radius of total circle
#define BOOST                 // boost the natural white by additional RGB



// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_GRBW    Pixels are wired for GRBW bitstream (f.e. SK6812)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUMPIXELS, NEO_PIN, NEO_PTYPE + NEO_KHZ800);



typedef struct {
  uint8_t   r;
  uint8_t   g;
  uint8_t   b;
  uint8_t   w;
} RGBW;



void setup() {
#ifdef __AVR_ATtiny85__
  // This is for Trinket 5V 16MHz
  if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
  // End of trinket special code
#endif

#ifdef LED_BUILTIN
  // Turn the onboard LED off by making the voltage LOW
  pinMode(LED_BUILTIN, OUTPUT); 
  digitalWrite(LED_BUILTIN, LOW);
#endif

  strip.begin();                    // This initializes the NeoPixel library.
  strip.setBrightness(BRIGHTNESS);  // set global brightness
  strip.show();                     // Initialize all pixels to 'off'

}



RGBW KtoRGB(uint32_t temp) {
  // pre-calculated via "precalculate-rgb.py" using the formulars from https://github.com/Susensio/RGBWLed/
  static const uint8_t rgb[3][256] PROGMEM = { // from 500 to 6000 Kelvin
    { // red
      255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
      255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
      255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
      255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
      255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
      255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
      255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
      255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    },
    { // green
      0, 3, 7, 11, 14, 18, 21, 25, 28, 31, 34, 37, 40, 43, 45, 48, 51, 53, 56, 58, 60, 63, 65, 67, 69, 71, 73, 75, 77, 79, 81, 83,
      85, 87, 88, 90, 92, 93, 95, 97, 98, 100, 101, 103, 104, 106, 107, 109, 110, 111, 113, 114, 116, 117, 118, 119, 121, 122, 123, 124, 126, 127, 128, 129,
      130, 131, 132, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 154, 155, 156, 157, 158, 159, 160, 160,
      161, 162, 163, 164, 165, 165, 166, 167, 168, 169, 169, 170, 171, 172, 172, 173, 174, 175, 175, 176, 177, 177, 178, 179, 180, 180, 181, 182, 182, 183, 184, 184,
      185, 186, 186, 187, 188, 188, 189, 189, 190, 191, 191, 192, 193, 193, 194, 194, 195, 196, 196, 197, 197, 198, 199, 199, 200, 200, 201, 201, 202, 202, 203, 204,
      204, 205, 205, 206, 206, 207, 207, 208, 208, 209, 209, 210, 210, 211, 211, 212, 212, 213, 213, 214, 214, 215, 215, 216, 216, 217, 217, 218, 218, 219, 219, 220,
      220, 221, 221, 221, 222, 222, 223, 223, 224, 224, 225, 225, 226, 226, 226, 227, 227, 228, 228, 229, 229, 229, 230, 230, 231, 231, 231, 232, 232, 233, 233, 233,
      234, 234, 235, 235, 235, 236, 236, 237, 237, 237, 238, 238, 239, 239, 239, 240, 240, 241, 241, 241, 242, 242, 242, 243, 243, 243, 244, 244, 245, 245, 245, 246,
    },
    { // blue
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 2, 6, 9, 12, 15, 18, 21, 23, 26, 29, 31, 34, 37, 39, 42, 44, 46, 49, 51, 53, 55, 58, 60, 62, 64, 66, 68, 70, 72, 74,
      76, 78, 80, 82, 83, 85, 87, 89, 90, 92, 94, 95, 97, 99, 100, 102, 103, 105, 107, 108, 110, 111, 113, 114, 115, 117, 118, 120, 121, 122, 124, 125,
      126, 128, 129, 130, 132, 133, 134, 135, 137, 138, 139, 140, 141, 143, 144, 145, 146, 147, 148, 149, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162,
      163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 180, 181, 182, 183, 184, 185, 186, 187, 187, 188, 189, 190, 191, 192,
      192, 193, 194, 195, 196, 196, 197, 198, 199, 200, 200, 201, 202, 203, 203, 204, 205, 206, 206, 207, 208, 209, 209, 210, 211, 212, 212, 213, 214, 214, 215, 216,
      216, 217, 218, 219, 219, 220, 221, 221, 222, 223, 223, 224, 224, 225, 226, 226, 227, 228, 228, 229, 230, 230, 231, 231, 232, 233, 233, 234, 235, 235, 236, 236,
    },
  };

  RGBW res;

  temp = constrain(temp, 500, 6000);
  temp = 255 * (temp - 500) / (6000 - 500);   // index in array
  temp = constrain(temp, 0, 255);

  res.r = pgm_read_word(&rgb[0][temp]);
  res.g = pgm_read_word(&rgb[1][temp]);
  res.b = pgm_read_word(&rgb[2][temp]);

  return res;
}



void replaceWhite(RGBW *res) {
  res->w = min( res->r, min(res->g, res->b) );
  res->r -= res->w;
  res->g -= res->w;
  res->b -= res->w;
  return;
}



void boostWhite(RGBW *res) {
  uint16_t x = max( res->r, max(res->g, res->b) );  // portion of color
  uint32_t scale = FLOAT * 510 / (x + res->w);      // scale to up to (255 white + 255 color)
  if( 255 < scale * x / FLOAT ) {                   // scale is limited by the portion of color
    scale = FLOAT * 255 / x;
  }
  // scale color
  res->r = res->r * scale / FLOAT;
  res->g = res->g * scale / FLOAT;
  res->b = res->b * scale / FLOAT;
  // scale white
  x = res->w * scale / FLOAT;         // total white
  res->w = min(x, 255);               // natural
  x -= res->w;                        // boosted white
  res->r += x;
  res->g += x;
  res->b += x;

  return;
}



uint16_t temperature(const uint32_t r) {
  if( r <= (NUMPIXELS-1)*UNITS_PER_PIXEL/2 ) return TEMP_CORE;
  else if( r >= R ) return TEMP_EDGE;
  else {
    return (TEMP_CORE - TEMP_EDGE) * (R - r)
            / (NUMTRANSITION * UNITS_PER_PIXEL)
            + TEMP_EDGE;
  }
}



// fading in
void brightness(uint32_t r, RGBW *res) {
  if( R - NUMFADE * UNITS_PER_PIXEL < r ) {    // dim
    r = R - r;
    res->r = res->r * r / (NUMFADE * UNITS_PER_PIXEL);
    res->g = res->g * r / (NUMFADE * UNITS_PER_PIXEL);
    res->b = res->g * r / (NUMFADE * UNITS_PER_PIXEL);
    res->w = res->w * r / (NUMFADE * UNITS_PER_PIXEL);
  }
  return;
}



void loop() {
  static const unsigned long t0 = millis(); // start of the sunrise
  static unsigned long offset = 0;          // lost milliseconds due to disabled interrupts

  unsigned long h = millis() + offset - t0; // current time
  if( h < DURATION ) {
    h = h/TIMESCALE * R / (DURATION/TIMESCALE);   // current height of the sun 
  } else {                                        // sunrise done
    h = R;
  }

  uint32_t x;
  RGBW color;
  for (uint16_t i=0; i<(NUMPIXELS+1)/2; i++) {  // one half only thanks to the symmetry

    x = (NUMPIXELS - 1) * UNITS_PER_PIXEL / 2 - i * UNITS_PER_PIXEL;    // pixel coordinate
    #ifdef TRIANGLE
    x += R - h;                                 // radius corresponding to x
    #else
    x = (uint32_t) sqrt( x*x + (R-h)*(R-h) );   // radius corresponding to x
    #endif

    if( x > R ) {
      // off
      strip.setPixelColor(i, 0, 0, 0, 0);
      strip.setPixelColor(NUMPIXELS-1-i, 0, 0, 0, 0);
    } else {
      // on
      color = KtoRGB( temperature(x) );   // RGB for current radius
      brightness(x, &color);              // fade in
      color.r = strip.gamma8(color.r);    // gamma correction
      color.g = strip.gamma8(color.g);
      color.b = strip.gamma8(color.b);
      replaceWhite(&color);               // use the white LED
      #ifdef BOOST
      boostWhite(&color);                 // boost white by RGB
      brightness(x, &color);              // restore fade in
      #endif
      strip.setPixelColor(i, color.r, color.g, color.b, color.w);
      strip.setPixelColor(NUMPIXELS-1-i, color.r, color.g, color.b, color.w);
    }
  }

  strip.show(); // millis() looses 40 micro-seconds per RGBW-pixel due to disabled interrupts
  offset += NUMPIXELS * 40 / 1000;
}
